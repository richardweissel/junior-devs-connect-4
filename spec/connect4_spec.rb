require_relative '../lib/connect4'

describe Conn do
  it "should check for a vertical win" do
    game = Conn.new
    game.mark_column(0, 'x')
    game.mark_column(0, 'x')
    game.mark_column(0, 'x')
    game.mark_column(0, 'x')
    expect(game.check(0, 'x')).to eq(true)
  end

  it "should check for a horizontal win" do
    game = Conn.new
    game.mark_column(0, 'x')
    game.mark_column(1, 'x')
    game.mark_column(2, 'x')
    game.mark_column(3, 'x')
    expect(game.check(3, 'x')).to eq(true)
  end
end
